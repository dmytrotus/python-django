#from django.http import HttpResponse
import datetime
from django.shortcuts import render

def todos_list(request):
    now = datetime.datetime.now()
    html = "<html><body>It is now %s.</body></html>" % now
    return render(request, "todos/templates/list.html", { "html": html, })
    #return HttpResponse(html)